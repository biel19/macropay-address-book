# Test: Macropay Address Book
You are developing a REST back end of an address book app. You were asked to implement endpoints to fetch a list of contacts and to fetch details of a single contact and to delete a given contact.

## Previous requirements
1. Install docker

        https://drive.google.com/file/d/1KloNhZffMbVpa6hIuldiq5N4HvCjCbd8/view?usp=share_link

2. Install docker-compose

        sudo apt install docker-compose

## Installation
1. Locate yourself on the root path of the project

        /path/macropay-address-book
2. Build the image with docker

        docker build -t macropay-address-book:0.0.1 .
3. Pull up the image with docker-compose

        docker-compose up -d

## Validate project
You can validate service from:
* http://127.0.0.1:3005/docs


!!! note

     If you want to perform tests from postman you need to send **Bearer Token** for Authorization,
