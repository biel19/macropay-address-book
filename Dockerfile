FROM python:3.11.2 AS builder
ENV DEBIAN_FRONTEND=noninteractive \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
RUN set -eux; \
    addgroup --gid 1000 macropay; \
    adduser --system --uid 1000 --gid 1000 macropay; \
    mkdir /opt/macropay; \
    chown 1000:1000 -R /opt/macropay
USER macropay
WORKDIR /opt/macropay
RUN set -eux; \
    python3 -m venv /opt/macropay; \
    /opt/macropay/bin/pip install --no-cache-dir --upgrade pip
RUN set -eux; \
    /opt/macropay/bin/pip install hatch==1.7.0
COPY --chown=1000:1000 . .
RUN set -eux; \
    /opt/macropay/bin/hatch build --clean

FROM python:3.11.2 AS stage
ENV DEBIAN_FRONTEND=noninteractive \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
RUN set -eux; \
    addgroup --gid 1000 macropay; \
    adduser --system --uid 1000 --gid 1000 macropay; \
    mkdir /opt/macropay; \
    chown 1000:1000 -R /opt/macropay
RUN set -eux; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y --no-install-recommends \
    libpq-dev \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*
USER macropay
WORKDIR /opt/macropay
RUN set -eux; \
    python3 -m venv /opt/macropay; \
    /opt/macropay/bin/pip install --no-cache-dir --upgrade pip
COPY --from=builder --chown=1000:1000 /opt/macropay/requirements.txt .
RUN set -eux; \
    /opt/macropay/bin/pip install -r requirements.txt
COPY --from=builder --chown=1000:1000 /opt/macropay/dist/ /opt/macropay/dist/
RUN set -eux; \
    /opt/macropay/bin/pip install --no-deps dist/*.whl; \
    find /opt/macropay/lib -type d -name tests -exec rm -rf '{}' +; \
    find /opt/macropay/lib -type d -name __pycache__ -exec rm -rf '{}' +; \
    rm -rf dist; \
    rm -rf requirements.txt
FROM python:3.11.2-slim AS deploy
ENV DEBIAN_FRONTEND=noninteractive \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
RUN set -eux; \
    addgroup --gid 1000 macropay; \
    adduser --system --uid 1000 --gid 1000 macropay; \
    mkdir /opt/macropay; \
    chown 1000:1000 -R /opt/macropay
RUN set -eux; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y --no-install-recommends \
    libpq-dev \
    ; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*
USER macropay
WORKDIR /opt/macropay
COPY --from=stage /opt/macropay /opt/macropay
ENV PATH="/opt/macropay/bin:${PATH}"
STOPSIGNAL SIGINT
EXPOSE 8000
CMD ["uvicorn", "macropay_address_book.main:app", "--host=0.0.0.0", "--port=3005"]
