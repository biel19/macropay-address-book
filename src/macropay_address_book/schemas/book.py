from pydantic import BaseModel


class BookBase(BaseModel):
    title: str
    author: str
    price: float
    availability: int
    num_reviews: int
    stars: int
    description: str
