from pydantic import BaseModel


class UserBase(BaseModel):
    user: str
    password: str
