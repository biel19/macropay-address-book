import json
from fastapi.encoders import jsonable_encoder
import uuid
from ..db.data import DUMMY_DATA

DATA = DUMMY_DATA # Constant containing dummy information


class CRUDBase():
    def __init__(self):
        self.model = DATA

    def get_multi_all(self):
        """
        Method to get all records
        """
        return self.model

    def get_multi(self, skip: int = 0, limit: int = 100):
        """
        Method to get multiple recorded with pagination
        """
        return self.model[skip:limit]

    def get(self, id_in):
        """
        Method to get a recorded
        """
        data = [element for element in self.model if element['id'] == id_in]
        return data

    def create(self, obj_in):
        """
        Method to create a new recorded
        """
        obj_in.update({"id": uuid.uuid4()})
        self.model.append(jsonable_encoder(obj_in))
        return obj_in
