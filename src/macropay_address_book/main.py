from fastapi import FastAPI
from .routes.api import router

app = FastAPI(
    title="Address Book",
    description="Service to Address Book",
    version="0.0.1"
)


@app.get("/hello")
async def root():
    return {"message": "Hello World"}

app.include_router(router)
