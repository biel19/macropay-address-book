from fastapi import APIRouter, HTTPException, status
from ..schemas.user import UserBase
from ..services.jwt_manager import create_access_token

router = APIRouter(
    prefix="/macropay/api/v1/users",
    tags=["Auth"],
    responses={404: {"description": "Not found"}}
)

USERS = {
    "user4": "pass4#"
} # Constant list to users

@router.post("/login", status_code=status.HTTP_201_CREATED)
async def post(payload: UserBase):
    """
    Endpoint for user authentication
    """
    user = USERS.get(payload.user)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="El usuario no existe"
        )
    if payload.password != 'pass4#':
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="El password no hace match"
        )
    access_token = create_access_token(data={"user_id": 1})

    return {"access_token": access_token}
