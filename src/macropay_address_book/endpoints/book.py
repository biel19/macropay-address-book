from fastapi import APIRouter, HTTPException, status, Depends
from ..crud.base import CRUDBase
from ..schemas.book import BookBase
import re
from ..services.book import string_similarity
from ..services.jwt_manager import JWTBearer

router = APIRouter(
    prefix="/macropay/api/v1/books",
    tags=["Book Managment"],
    responses={404: {"description": "Not found"}}
)


@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    dependencies=[Depends(JWTBearer())]
)
async def post(payload: BookBase):
    """
    Endpoint to create a new recorded
    """
    obj_crud = CRUDBase()
    # Variable to indicate that field will check in the query.
    fields = payload.model_dump()
    register = obj_crud.create(obj_in=fields)
    return {"detail": register}


@router.get(
    "",
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(JWTBearer())]
)
async def get(price: int | None = None, phrase: str | None = None):
    """
    Endpoint to returning multiple data
    """
    obj_crud = CRUDBase()
    data = obj_crud.get_multi_all()
    if price:
        # if exist attribute price
        result = [element for element in data if element['price'] >= price]
    elif phrase:
        # if exist attribute phrase
        result = [element for element in data if string_similarity(
            element['author'], phrase) > 17]
    else:
        result = data

    if not result:
        # When data is empty
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Sin registros"
        )

    return {"detail": result, 'size': len(result)}


@router.get(
    "/{id}",
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(JWTBearer())]
)
async def get(
    id: str,
):
    """
    Endpoint to returning in a specifict recorded
    """
    obj_crud = CRUDBase()
    result = obj_crud.get(id_in=id)
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Registro no encontrado con el id {id}"
        )
    return {"detail": result}


@router.get(
    "/average/",
    tags=["The average cost of each book"],
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(JWTBearer())]
)
async def get():
    """
    Endpoint to returning average
    """
    obj_crud = CRUDBase()
    result = obj_crud.get_multi_all()
    price_list = [value['price'] for value in result]
    average = sum(price_list) / len(price_list)
    return {"detail": round(average, 2)}
