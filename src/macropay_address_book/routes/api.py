from fastapi import APIRouter
from ..endpoints import book, user

router = APIRouter()
router.include_router(user.router)
router.include_router(book.router)
