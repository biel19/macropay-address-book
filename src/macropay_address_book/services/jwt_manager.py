from fastapi import Request, HTTPException, status
import jwt
from datetime import timedelta, datetime
from fastapi.security import HTTPBearer

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ACCESS_TOKEN_EXPIRE_MINUTES = 60


def create_access_token(data: dict):
    """
    Function to create access token
    """
    to_encode = data.copy()
    expire = datetime.now() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"expire": expire.strftime("%Y-%m-%d %H:%M:%S")})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, "HS256")
    return encoded_jwt


def validate_access_token(token: str):
    """
    Function to validate access token
    """
    try:
        token_data: dict = jwt.decode(token, SECRET_KEY, ["HS256"])

    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=str(e)
        )
    return token_data


class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_access_token(auth.credentials)
        if data['user_id'] != 1:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Credenciales son invalidas"
            )
        date_str = data['expire']
        date_format = '%Y-%m-%d %H:%M:%S'
        date_obj = datetime.strptime(date_str, date_format)
        if date_obj < datetime.now() :
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="El token expiró"
            )
