import difflib


def string_similarity(str1, str2):
    """
    function to calculate the similarity ratio between two strings
    """
    result = difflib.SequenceMatcher(a=str1.lower(), b=str2.lower())
    return result.ratio() * 100
